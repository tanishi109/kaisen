﻿using Kaisen.Domain.UseCase;
using Kaisen.Presentation.Presenter;
using Kaisen.Presentation.View.Title;
using UnityEngine;
using Zenject;

namespace Kaisen.Application.Installer.Scene
{
    public class TitleInstaller : MonoInstaller<TitleInstaller>
    {
        [SerializeField] private ButtonStart _buttonStart;
        private ButtonStart ButtonStart => _buttonStart;

        [SerializeField] private ButtonStartMulti _buttonStartMulti;
        private ButtonStartMulti ButtonStartMulti => _buttonStartMulti;

        public override void InstallBindings()
        {
            // UseCases
            Container.BindInterfacesTo<TitleNavigationUseCase>().AsCached();

            // Presenters
            Container.BindInterfacesTo<TitlePresenter>().AsCached();

            // Views
            Container.Bind<IButtonTrigger>().WithId(Constant.InjectId.ButtonStart).FromInstance(ButtonStart)
                .AsCached();
            Container.Bind<IButtonTrigger>().WithId(Constant.InjectId.ButtonStartMulti).FromInstance(ButtonStartMulti)
                .AsCached();
        }
    }
}