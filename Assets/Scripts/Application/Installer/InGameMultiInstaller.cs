﻿using Kaisen.Domain.UseCase;
using Zenject;

namespace Kaisen.Application.Installer.Scene
{
    public class InGameMultiInstaller : MonoInstaller<InGameMultiInstaller>
    {
        public override void InstallBindings()
        {
            // UseCase
            Container.BindInterfacesTo<InGameMultiUseCase>().AsCached();
        }
    }
}