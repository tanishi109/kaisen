﻿using ExtraUniRx;
using Kaisen.Domain.Entity;
using Kaisen.Domain.Structure;
using Kaisen.Domain.Translator;
using Kaisen.Domain.UseCase;
using Kaisen.Presentation.Presenter;
using Kaisen.Presentation.View.Game;
using Kaisen.Presentation.View.InGame;
using UnityEngine;
using Zenject;

namespace Kaisen.Application.Installer.Scene
{
    public class InGameInstaller : MonoInstaller<InGameInstaller>
    {
        [SerializeField] private SushiView sushiPrefab;
        private SushiView SushiPrefab => sushiPrefab;

        [SerializeField] private Transform sushiParent;
        private Transform SushiParent => sushiParent;

        [SerializeField] private Score score;
        private Score Score => score;

        [SerializeField] private Timer timer;
        private Timer Timer => timer;

        [SerializeField] private KiView kiPrefab;
        private KiView KiPrefab => kiPrefab;

        [SerializeField] private GyroView gyroView;
        private GyroView GyroView => gyroView;

        [SerializeField] private ButtonBack _buttonBack;
        private ButtonBack ButtonBack => _buttonBack;

        public override void InstallBindings()
        {
            Container.Bind<IInGameEntity>().To<InGameEntity>().AsCached();
            Container.BindInterfacesTo<ScoreEntity>().AsCached();
            Container.BindInterfacesTo<GyroEntity>().AsCached();
            Container.BindInterfacesTo<InputEntity>().AsCached();
            // SushiEntity は Factory 経由で生成
            Container.BindIFactory<int, ISushiEntity>().To<SushiEntity>();


            // Translators
            Container.BindInterfacesTo<SushiTranslator>().AsCached();
            Container.BindInterfacesTo<GyroTranslator>().AsCached();

            // UseCases
            Container.BindInterfacesTo<InGameUseCase>().AsCached();
            Container.BindInterfacesTo<SushiUseCase>().AsCached();
            Container.BindInterfacesTo<KiUseCase>().AsCached();
            Container.BindInterfacesTo<GyroUseCase>().AsCached();
            Container.BindInterfacesTo<InGameNavigationUseCase>().AsCached();

            // Presenters
            Container.BindInterfacesTo<InGamePresenter>().AsCached();
            Container.BindInterfacesTo<SushiPresenter>().AsCached();
            Container.BindInterfacesTo<KiPresenter>().AsCached();
            Container.BindInterfacesTo<GyroPresenter>().AsCached();

            // Views
            Container.BindInterfacesTo<Score>().FromInstance(Score).AsCached();
            Container.BindInterfacesTo<Timer>().FromInstance(Timer).AsCached();
            Container
                // ISushiView の Factory を登録
                .BindIFactory<ISushi, ISushiView>()
                // Sushi 型として
                .To<SushiView>()
                // SushiPrefab 内のコンポーネントを用いて
                .FromComponentInNewPrefab(SushiPrefab)
                // SushiParent の配下に
                .UnderTransform(SushiParent);
            Container
                .BindIFactory<string, Vector2, Transform, ITenseSubject<string>, IKiView>()
                .To<KiView>()
                .FromComponentInNewPrefab(KiPrefab);
            Container.Bind<IGyroView>().FromInstance(GyroView).AsCached();
            Container.Bind<IButtonTrigger>().WithId(Constant.InjectId.ButtonBack).FromInstance(ButtonBack)
                .AsCached();
        }

        public static void WorkAround()
        {
            new PlaceholderFactory<int, ISushiEntity>();
            new PlaceholderFactory<ISushi, ISushiView>();
            new PlaceholderFactory<string, Vector2, Transform, ITenseSubject<string>, IKiView>();
        }
    }
}