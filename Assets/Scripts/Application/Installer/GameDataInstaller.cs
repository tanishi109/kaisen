﻿using Kaisen.Domain.Entity;
using Kaisen.Domain.UseCase;
using Zenject;

namespace Kaisen.Application.Installer.Scene
{
    public class GameDataInstaller : MonoInstaller<GameDataInstaller>
    {
        public override void InstallBindings()
        {
            // Entity
            Container.BindIFactory<float, IPlayerSettingEntity>().To<PlayerSettingEntity>();
            Container.BindInterfacesTo<GameSettingEntity>().AsCached();

            // UseCase
            Container.BindInterfacesTo<GameSettingUseCase>().AsCached();
        }
    }
}