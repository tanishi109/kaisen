﻿namespace Kaisen.Application.Enumerate
{
    public enum InGameEventName
    {
        Start,
        Finish,
    }
}