﻿namespace Kaisen.Application.Enumerate
{
    public enum SushiEventName
    {
        Deactivate,
        Hide,
        Increment,
        SubmitIncrement,
        Cooked,
        OverCooked,
        UnderCooked,
        DishUp,
    }
}