﻿namespace Kaisen.Application.Enumerate
{
    public enum SceneName
    {
        System,
        KaisenTitle,
        KaisenInGame,
        KaisenPrepareMulti,
        KaisenInGameMulti,
        KaisenGameData,
    }
}