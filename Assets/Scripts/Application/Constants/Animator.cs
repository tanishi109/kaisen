﻿namespace Kaisen.Application
{
    public static partial class Constant
    {
        public static class TriggerName
        {
            public const string Cooked = "Cooked";
            public const string OverCooked = "OverCooked";
            public const string UnderCooked = "UnderCooked";
            public const string KiDespawn = "KiDespawn";
        }

        public static class AnimationStateName
        {
            public const string Cooked = "Cooked";
            public const string OverCooked = "OverCooked";
            public const string UnderCooked = "UnderCooked";
            public const string KiDespawn = "KiDespawn";
        }
    }
}