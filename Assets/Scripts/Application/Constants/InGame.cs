﻿namespace Kaisen.Application
{
    public static partial class Constant
    {
        public static class InGame
        {
            public const float GameTimer = 0;
            public const float ScoreLimit = 10;
        }
    }
}