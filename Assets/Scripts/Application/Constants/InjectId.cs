﻿namespace Kaisen.Application
{
    public static partial class Constant
    {
        public static class InjectId
        {
            public const string ButtonStart = "ButtonStart";
            public const string ButtonStartMulti = "ButtonStartMulti";
            public const string ButtonBack = "ButtonBack";
        }
    }
}