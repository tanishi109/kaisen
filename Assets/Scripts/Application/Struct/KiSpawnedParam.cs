﻿using System;
using ExtraUniRx;
using UnityEngine;

namespace Kaisen.Application.Struct
{
    [Serializable]
    public class KiSpawnedParam
    {
        public string Id { get; }
        public Vector2 Position { get; set; }
        public Transform Parent { get; }
        public ITenseSubject<string> KiDespawnSubject { get; }

        public KiSpawnedParam(string id, Vector2 position, Transform parent, ITenseSubject<string> kiDespawnSubject)
        {
            Id = id;
            Position = position;
            Parent = parent;
            KiDespawnSubject = kiDespawnSubject;
        }
    }
}