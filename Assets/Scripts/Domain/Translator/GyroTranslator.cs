﻿using CAFU.Core;
using Kaisen.Domain.Entity;
using Kaisen.Domain.Structure;

namespace Kaisen.Domain.Translator
{
    public class GyroTranslator : ITranslator<IGyroEntity, IGyroStructure>
    {
        public IGyroStructure Translate(IGyroEntity param1)
        {
            // BindIFactory は型パラメータ 0〜6, 10個 という制限あり
            return new GyroStructure(
                param1.GyroAttitudeSubject
            );
        }
    }
}