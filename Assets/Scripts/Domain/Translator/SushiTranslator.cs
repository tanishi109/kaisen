﻿using CAFU.Core;
using Kaisen.Domain.Entity;
using Kaisen.Domain.Structure;

namespace Kaisen.Domain.Translator
{
    public class SushiTranslator : ITranslator<ISushiEntity, IInGameEntity, IInputEntity, ISushi>
    {
        public ISushi Translate(ISushiEntity param1, IInGameEntity param2, IInputEntity param3)
        {
            // BindIFactory は型パラメータ 0〜6, 10個 という制限あり
            return new Sushi(
                param1.Number.Value,
                param1.EventSubject,
                param2.KiSpawnedSubject,
                param2.KiDespawnSubject,
                param3.EventSubject
            );
        }
    }
}