﻿using CAFU.Core;
using CAFU.Scene.Domain.Entity;
using Kaisen.Application.Enumerate;
using UniRx;
using Zenject;
using UnityEngine;

namespace Kaisen.Domain.UseCase
{
    public class TitleNavigationUseCase : IUseCase, IInitializable
    {
        [Inject] private ITitleNavigator TitleNavigator { get; set; }
        [Inject] private IRequestEntity RequestEntity { get; set; }

        void IInitializable.Initialize()
        {
            TitleNavigator.OnNavigateToInGameAsObservable().Subscribe(_ => NavigateToInGame());
            TitleNavigator.OnNavigateToPrepareMultiAsObservable().Subscribe(_ => NavigateToInGameMulti());
        }

        private void NavigateToInGame()
        {
            RequestEntity.RequestLoad(SceneName.KaisenInGame.ToString());
            RequestEntity.RequestUnload(SceneName.KaisenTitle.ToString());
        }

        private void NavigateToInGameMulti()
        {
            RequestEntity.RequestLoad(SceneName.KaisenPrepareMulti.ToString());
            RequestEntity.RequestUnload(SceneName.KaisenTitle.ToString());
        }
    }
}