﻿using System.Collections.Generic;
using CAFU.Core;
using ExtraUniRx;
using Kaisen.Application.Enumerate;
using Kaisen.Domain.Entity;
using Kaisen.Domain.Structure;
using UniRx;
using Zenject;

namespace Kaisen.Domain.UseCase
{
    public interface IGameSettingUseCase : IUseCase
    {
    }

    public class GameSettingUseCase : IGameSettingUseCase, IInitializable
    {
        [Inject] private IGameSettingEntity GameSettingEntity { get; set; }
        [Inject] private IFactory<float, IPlayerSettingEntity> PlayerSettingFactory { get; set; }

        public void Initialize()
        {
            // TODO: ここはインプットから生成
            this.GameSettingEntity.PlayerSettings = new List<IPlayerSettingEntity>()
            {
                PlayerSettingFactory.Create(0),
                PlayerSettingFactory.Create(180),
            };
        }
    }
}