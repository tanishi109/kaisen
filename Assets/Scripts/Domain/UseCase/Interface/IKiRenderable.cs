﻿using CAFU.Core;
using Kaisen.Application.Struct;

namespace Kaisen.Domain.UseCase
{
    public interface IKiRenderable : IPresenter
    {
        void RenderKi(KiSpawnedParam param);
    }
}