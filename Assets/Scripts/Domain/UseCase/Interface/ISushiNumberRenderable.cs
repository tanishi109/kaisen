﻿using CAFU.Core;

namespace Kaisen.Domain.UseCase
{
    // TODO: ファイル名ちがくね
    public interface IGameScoreRenderable : IPresenter
    {
        void RenderScore(int score);
    }
}