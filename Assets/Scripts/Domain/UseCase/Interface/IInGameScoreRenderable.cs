﻿using CAFU.Core;

namespace Kaisen.Domain.UseCase
{
    // TODO: fix with sushi number renderable
    public interface IInGameScoreRenderable : IPresenter
    {
        void RenderScore(int score);
    }
}