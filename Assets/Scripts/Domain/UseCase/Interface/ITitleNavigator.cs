﻿using System;
using UniRx;
using IPresenter = CAFU.Core.IPresenter;

namespace Kaisen.Domain.UseCase
{
    public interface ITitleNavigator : IPresenter
    {
        IObservable<Unit> OnNavigateToInGameAsObservable();
        IObservable<Unit> OnNavigateToPrepareMultiAsObservable();
    }
}