﻿using CAFU.Core;
using Kaisen.Domain.Structure;

namespace Kaisen.Domain.UseCase
{
    public interface IGyroPresenter : IPresenter
    {
        void Instantiate(IGyroStructure gyroStructure);
        void Render(float gyroAttitude);
    }
}