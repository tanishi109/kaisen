﻿using CAFU.Core;
using Kaisen.Domain.Structure;

namespace Kaisen.Domain.UseCase
{
    public interface ISushiPresenter : IPresenter
    {
        void Instantiate(ISushi sushi);
        void RenderNumber(int number);
    }
}