﻿using System;
using UniRx;
using IPresenter = CAFU.Core.IPresenter;

namespace Kaisen.Domain.UseCase
{
    public interface IInGameNavigator : IPresenter
    {
        IObservable<Unit> OnNavigateToTitleAsObservable();
    }
}