﻿using CAFU.Core;

namespace Kaisen.Domain.UseCase
{
    public interface ITimerRenderable : IPresenter
    {
        void RenderTimer(float time);
    }
}