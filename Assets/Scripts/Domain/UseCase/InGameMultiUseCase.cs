﻿using System;
using CAFU.Core;
using CAFU.Scene.Domain.Entity;
using ExtraUniRx;
using Kaisen.Application;
using Kaisen.Application.Enumerate;
using Kaisen.Domain.Entity;
using Kaisen.Presentation.View.Game;
using UniRx;
using UnityEngine;
using Zenject;

namespace Kaisen.Domain.UseCase
{
    public interface IInGameMultiUseCase : IUseCase
    {
    }

    public class InGameMultiUseCase : IInGameMultiUseCase, IInitializable
    {
        [Inject] private IGameSettingEntity GameSettingEntity { get; set; }

        void IInitializable.Initialize()
        {
            Debug.Log("inGameMultiUseCase");

            Debug.LogFormat("degree0 =  {0}", GameSettingEntity.PlayerSettings[0].Degree);
            Debug.LogFormat("degree1 =  {0}", GameSettingEntity.PlayerSettings[1].Degree);
        }
    }
}