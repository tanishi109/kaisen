﻿using CAFU.Core;
using CAFU.Scene.Domain.Entity;
using Kaisen.Application.Enumerate;
using UniRx;
using Zenject;
using UnityEngine;

namespace Kaisen.Domain.UseCase
{
    public class InGameNavigationUseCase : IUseCase, IInitializable
    {
        [Inject] private IInGameNavigator InGameNavigator { get; set; }
        [Inject] private IRequestEntity RequestEntity { get; set; }

        void IInitializable.Initialize()
        {
            InGameNavigator.OnNavigateToTitleAsObservable().Subscribe(_ => NavigateToTitle());
        }

        private void NavigateToTitle()
        {
            RequestEntity.RequestLoad(SceneName.KaisenTitle.ToString());
            RequestEntity.RequestUnload(SceneName.KaisenInGame.ToString());
        }
    }
}