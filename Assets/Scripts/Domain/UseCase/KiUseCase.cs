﻿using CAFU.Core;
using ExtraUniRx;
using Kaisen.Domain.Entity;
using UniRx;
using UnityEngine;
using Zenject;

namespace Kaisen.Domain.UseCase
{
    public class KiUseCase : IUseCase, IInitializable
    {
        [Inject] private IKiRenderable KiRenderable { get; set; }
        [Inject] private IInGameEntity InGameEntity { get; set; }

        void IInitializable.Initialize()
        {
            InGameEntity
                .KiSpawnedSubject
                .WhenDo()
                .Subscribe(param => KiRenderable.RenderKi(param));
        }
    }
}