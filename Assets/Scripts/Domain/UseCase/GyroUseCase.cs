﻿using CAFU.Core;
using ExtraUniRx;
using Kaisen.Application.Enumerate;
using Kaisen.Domain.Entity;
using Kaisen.Domain.Structure;
using UniRx;
using Zenject;

namespace Kaisen.Domain.UseCase
{
    public interface IGyroUseCase : IUseCase
    {
    }

    public class GyroUseCase : IGyroUseCase, IInitializable
    {
        [Inject] private IGyroEntity GyroEntity { get; set; }
        [Inject] private ITranslator<IGyroEntity, IGyroStructure> GyroStructureTranslator { get; set; }
        [Inject] private IGyroPresenter GyroPresenter { get; set; }
        [Inject] private IInputEntity InputEntity { get; set; }

        public void Initialize()
        {
            GyroPresenter.Instantiate(GyroStructureTranslator.Translate(GyroEntity));

            GyroEntity
                .Current
                .Subscribe(GyroPresenter.Render);

            GyroEntity
                .GyroEventSubject
                .WhenDo()
                .Where(e => e == GyroEventName.OnThreshold)
                .Subscribe(_ => InputEntity.EventSubject.Do(InputEventName.Submit));
        }
    }
}