﻿using CAFU.Core;
using ExtraUniRx;
using Kaisen.Application;
using Kaisen.Application.Enumerate;
using Kaisen.Domain.Entity;
using Kaisen.Domain.Structure;
using Kaisen.Presentation.Presenter;
using UniRx;
using Zenject;
using UnityEngine;

namespace Kaisen.Domain.UseCase
{
    public interface ISushiUseCase : IUseCase
    {
    }

    public class SushiUseCase : ISushiUseCase, IInitializable
    {
        [Inject] private IFactory<int, ISushiEntity> SushiEntityFactory { get; set; }

        [Inject]
        private ITranslator<ISushiEntity, IInGameEntity, IInputEntity, ISushi> SushiStructureTranslator { get; set; }

        [Inject] private ISushiPresenter SushiPresenter { get; set; }
        [Inject] private IInGameEntity InGameEntity { get; set; }
        [Inject] private IInputEntity InputEntity { get; set; }
        [Inject] private IScoreEntity ScoreEntity { get; set; }

        public void Initialize()
        {
            SpawnSushi();
        }

        private void InitializeSushi(ISushiEntity sushiEntity)
        {
            // View の生成
            SushiPresenter.Instantiate(SushiStructureTranslator.Translate(sushiEntity, InGameEntity, InputEntity));

            // ゲーム全体のステータスに連動した処理を登録
//            GameStateEntity.WillStartSubject.Subscribe(_ => sushiEntity.Start());
//            GameStateEntity.WillFinishSubject.Subscribe(_ => sushiEntity.Finish());


            sushiEntity
                .Number
                .Subscribe(SushiPresenter.RenderNumber);

            sushiEntity.EventSubject
                .WhenDo()
                .Where(e => e == SushiEventName.Cooked)
                .Subscribe(_ => { InGameEntity.GetScoreSubject.Do(); });

            sushiEntity.EventSubject
                .WhenDid()
                .Where(e => e == SushiEventName.OverCooked)
                .Subscribe(_ => { SpawnSushi(); });

            sushiEntity.EventSubject
                .WhenDid()
                .Where(e => e == SushiEventName.UnderCooked)
                .Subscribe(_ => { SpawnSushi(); });

            sushiEntity.EventSubject
                .WhenDid()
                .Where(e => e == SushiEventName.Cooked)
                .Subscribe(_ => { SpawnSushi(); });
        }

        private void SpawnSushi()
        {
            if (ScoreEntity.Current.Value >= Constant.InGame.ScoreLimit)
            {
                return;
            }

            var number = Random.Range(0, 9);
            InitializeSushi(SushiEntityFactory.Create(number));
        }
    }
}