﻿using System;
using CAFU.Core;
using CAFU.Scene.Domain.Entity;
using ExtraUniRx;
using Kaisen.Application;
using Kaisen.Application.Enumerate;
using Kaisen.Domain.Entity;
using Kaisen.Presentation.View.Game;
using UniRx;
using UnityEngine;
using Zenject;

namespace Kaisen.Domain.UseCase
{
    public interface IInGameUseCase : IUseCase
    {
    }

    public class InGameUseCase : IInGameUseCase, IInitializable
    {
        [Inject] private IScoreEntity ScoreEntity { get; set; }
        [Inject] private IGameScoreRenderable GameScoreRenderable { get; set; }
        [Inject] private ITimerRenderable TimerRenderable { get; set; }
        [Inject] private IInGameEntity InGameEntity { get; set; }

        private IDisposable GameTimerSubscription { get; set; }

        void IInitializable.Initialize()
        {
            ScoreEntity
                .Current
                .Subscribe(GameScoreRenderable.RenderScore);

            InGameEntity
                .GameTimer
                .Subscribe(TimerRenderable.RenderTimer);

            InGameEntity
                .GetScoreSubject
                .WhenDo()
                .Subscribe(_ => { ScoreEntity.Increment(); });

            ScoreEntity
                .Current
                .Where(s => s >= Constant.InGame.ScoreLimit)
                .First()
                .Subscribe(_ => StopGame());

            InGameEntity
                .InGameEventSubject
                .WhenWill()
                .Where(e => e == InGameEventName.Start)
                .Subscribe(_ => StartGame());
            InGameEntity
                .InGameEventSubject
                .WhenWill()
                .Where(e => e == InGameEventName.Finish)
                .Subscribe(_ => FinishGame());

            Observable
                .Timer(TimeSpan.FromSeconds(1.0)) // 1秒後にゲームスタート
                .AsUnitObservable()
                .Subscribe(_ => { InGameEntity.InGameEventSubject.Will(InGameEventName.Start); });
        }

        private void StartGame()
        {
            InGameEntity.GameTimer.Value = Constant.InGame.GameTimer;
            GameTimerSubscription = Observable
                .EveryUpdate()
                .Subscribe(_ => InGameEntity.GameTimer.Value += Time.deltaTime);
        }

        private void StopGame()
        {
            GameTimerSubscription?.Dispose();

            InGameEntity.InGameEventSubject.Will(InGameEventName.Finish);
        }

        private void FinishGame()
        {
            Debug.Log("Game finished!!");
        }
    }
}