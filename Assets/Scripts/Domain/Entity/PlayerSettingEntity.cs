﻿using CAFU.Core;
using UniRx;

namespace Kaisen.Domain.Entity
{
    public interface IPlayerSettingEntity : IEntity
    {
        IReactiveProperty<float> Degree { get; }
    }

    public class PlayerSettingEntity : IPlayerSettingEntity
    {
        public IReactiveProperty<float> Degree { get; } = new FloatReactiveProperty();

        public PlayerSettingEntity(float degree)
        {
            Degree.Value = degree;
        }
    }
}