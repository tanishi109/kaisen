﻿using CAFU.Core;
using ExtraUniRx;
using Kaisen.Application;
using Kaisen.Application.Enumerate;
using Kaisen.Application.Struct;
using UniRx;

namespace Kaisen.Domain.Entity
{
    public interface IInGameEntity : IEntity
    {
        ITenseSubject GetScoreSubject { get; }
        ITenseSubject<KiSpawnedParam> KiSpawnedSubject { get; }
        ITenseSubject<string> KiDespawnSubject { get; }
        ITenseSubject<InGameEventName> InGameEventSubject { get; }
        IReactiveProperty<float> GameTimer { get; }
    }

    public class InGameEntity : IInGameEntity
    {
        public ITenseSubject GetScoreSubject { get; } = new TenseSubject();
        public ITenseSubject<KiSpawnedParam> KiSpawnedSubject { get; } = new TenseSubject<KiSpawnedParam>();
        public ITenseSubject<string> KiDespawnSubject { get; } = new TenseSubject<string>();
        public ITenseSubject<InGameEventName> InGameEventSubject { get; } = new TenseSubject<InGameEventName>();
        public IReactiveProperty<float> GameTimer { get; } = new FloatReactiveProperty(Constant.InGame.GameTimer);
    }
}