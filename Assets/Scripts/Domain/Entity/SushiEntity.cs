﻿using CAFU.Core;
using ExtraUniRx;
using Kaisen.Application.Enumerate;
using UniRx;
using UnityEngine;
using Zenject;

namespace Kaisen.Domain.Entity
{
    public interface ISushiEntity : IEntity
    {
        ITenseSubject<SushiEventName> EventSubject { get; }
        IReactiveProperty<int> Number { get; }

        void Start();
        void Finish();
        void Increment();
    }

    public class SushiEntity : ISushiEntity
    {
        public ITenseSubject<SushiEventName> EventSubject { get; } = new TenseSubject<SushiEventName>();
        public IReactiveProperty<int> Number { get; } = new IntReactiveProperty();
        private bool _isEnabled = true;

        public void Start()
        {
        }

        public void Finish()
        {
        }

        public SushiEntity(int num)
        {
            Number.Value = num;
        }

        public void Increment()
        {
            // TODO: spawn +1 Object
        }

        public void SubmitIncrement()
        {
            Number.Value++;

            if (Number.Value > 10)
            {
                EventSubject.Will(SushiEventName.OverCooked);
            }
        }

        private void DishUp()
        {
            if (Number.Value == 10)
            {
                EventSubject.Will(SushiEventName.Cooked);
            }

            if (Number.Value > 10)
            {
                Debug.LogWarning("*** OverCooked called?"); // ここは通らないはず
                EventSubject.Will(SushiEventName.OverCooked);
            }

            if (Number.Value < 10)
            {
                Debug.Log("*** UnderCooked");
                EventSubject.Will(SushiEventName.UnderCooked);
            }
        }

        [Inject]
        public void Initialize()
        {
            EventSubject
                .WhenDo()
                .Where(e => e == SushiEventName.Increment)
                .Where(_ => _isEnabled)
                .Subscribe(_ => Increment());

            EventSubject
                .WhenDo()
                .Where(e => e == SushiEventName.SubmitIncrement)
                .Where(_ => _isEnabled)
                .Subscribe(_ => SubmitIncrement());

            EventSubject
                .WhenDo()
                .Where(e => e == SushiEventName.DishUp)
                .Where(_ => _isEnabled)
                .Subscribe(_ => DishUp());

            EventSubject
                .WhenWill()
                .Where(e => e == SushiEventName.Cooked)
                .Subscribe(_ =>
                {
                    EventSubject.Do(SushiEventName.Deactivate);
                    EventSubject.Do(SushiEventName.Cooked);
                });

            EventSubject
                .WhenWill()
                .Where(e => e == SushiEventName.OverCooked)
                .Subscribe(_ =>
                {
                    // TODO: Deactivateしてることで11より大きくならないので、強制Submitしていくつタップしすぎていたか気づけるようにできるといい
                    EventSubject.Do(SushiEventName.Deactivate);
                    EventSubject.Do(SushiEventName.OverCooked);
                });

            EventSubject
                .WhenWill()
                .Where(e => e == SushiEventName.UnderCooked)
                .Subscribe(_ =>
                {
                    EventSubject.Do(SushiEventName.Deactivate);
                    EventSubject.Do(SushiEventName.UnderCooked);
                });

            EventSubject
                .WhenDo()
                .Where(e => e == SushiEventName.Deactivate)
                .Subscribe(_ => _isEnabled = false);
        }
    }
}