﻿using CAFU.Core;
using ExtraUniRx;
using Kaisen.Application.Enumerate;
using Zenject;

namespace Kaisen.Domain.Entity
{
    public interface IInputEntity : IEntity
    {
        ITenseSubject<InputEventName> EventSubject { get; }
    }

    public class InputEntity : IInputEntity
    {
        public ITenseSubject<InputEventName> EventSubject { get; } = new TenseSubject<InputEventName>();

        public InputEntity()
        {
        }

        [Inject]
        public void Initialize()
        {
        }
    }
}