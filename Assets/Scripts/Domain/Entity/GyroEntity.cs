﻿using System;
using CAFU.Core;
using ExtraUniRx;
using Kaisen.Application.Enumerate;
using UniRx;
using UnityEngine;
using Zenject;

namespace Kaisen.Domain.Entity
{
    public interface IGyroEntity : IEntity
    {
        float Initial { get; }
        float Threshold { get; }
        float InitialRange { get; }
        IReactiveProperty<float> Current { get; }
        ITenseSubject<float> GyroAttitudeSubject { get; }
        ITenseSubject<GyroEventName> GyroEventSubject { get; }
    }

    public class GyroEntity : IGyroEntity
    {
        public float Initial { get; } = 0.0f;
        public float Threshold { get; } = 0.0f;
        public float InitialRange { get; } = 0.1f;
        public IReactiveProperty<float> Current { get; } = new FloatReactiveProperty();
        public ITenseSubject<float> GyroAttitudeSubject { get; } = new TenseSubject<float>();
        public ITenseSubject<GyroEventName> GyroEventSubject { get; } = new TenseSubject<GyroEventName>();
        private bool _thresholdEnabled = true;

        public GyroEntity()
        {
            Initial = 0.0f;
            Threshold = 0.4f; // TODO: 認識する傾き量を変更できるようにする
        }

        [Inject]
        public void Initialize()
        {
            GyroAttitudeSubject
                .WhenDo()
                .Subscribe(n => { Current.Value = n; });

            Current
                .Subscribe(n =>
                {
                    if (Mathf.Abs(n) > Threshold && _thresholdEnabled)
                    {
                        GyroEventSubject.Do(GyroEventName.OnThreshold);
                        _thresholdEnabled = false;
                    }
                });
            Current
                .Subscribe(n =>
                {
                    if (Initial - InitialRange <= Mathf.Abs(n) && Math.Abs(n) <= Initial + InitialRange)
                    {
                        _thresholdEnabled = true;
                    }
                });
        }
    }
}