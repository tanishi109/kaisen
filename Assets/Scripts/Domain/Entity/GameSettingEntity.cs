﻿using System.Collections.Generic;
using CAFU.Core;

namespace Kaisen.Domain.Entity
{
    public interface IGameSettingEntity : IEntity
    {
        List<IPlayerSettingEntity> PlayerSettings { get; set; }
    }

    public class GameSettingEntity : IGameSettingEntity
    {
        public List<IPlayerSettingEntity> PlayerSettings { get; set; } = new List<IPlayerSettingEntity>();

        public GameSettingEntity(List<IPlayerSettingEntity> playerSettings)
        {
            PlayerSettings = playerSettings;
        }
    }
}