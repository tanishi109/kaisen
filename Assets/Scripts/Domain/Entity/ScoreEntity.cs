﻿using CAFU.Core;
using UniRx;
using UnityEngine;

namespace Kaisen.Domain.Entity
{
    public interface IScoreEntity : IEntity
    {
        IReactiveProperty<int> Current { get; }
        void Increment();
    }

    public class ScoreEntity : IScoreEntity
    {
        public IReactiveProperty<int> Current { get; } = new IntReactiveProperty();

        public void Increment()
        {
            Debug.Log("scoreEntity Increment() called!");
            Current.Value++;
        }
    }
}