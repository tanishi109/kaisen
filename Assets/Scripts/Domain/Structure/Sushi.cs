﻿using CAFU.Core;
using ExtraUniRx;
using Kaisen.Application.Enumerate;
using Kaisen.Application.Struct;
using Kaisen.Domain.Entity;
using UniRx;

namespace Kaisen.Domain.Structure
{
    public interface ISushi : IStructure
    {
        int Number { get; }
        ITenseSubject<SushiEventName> EventSubject { get; }
        ITenseSubject<KiSpawnedParam> KiSpawnedSubject { get; }
        ITenseSubject<string> KiDespawnSubject { get; }
        ITenseSubject<InputEventName> InputEventSubject { get; }
    }

    public struct Sushi : ISushi
    {
        public Sushi(int number, ITenseSubject<SushiEventName> eventSubject,
            ITenseSubject<KiSpawnedParam> kiSpawnedSubject, ITenseSubject<string> kiDespawnSubject,
            ITenseSubject<InputEventName> inputSubject)
        {
            Number = number;
            EventSubject = eventSubject;
            KiSpawnedSubject = kiSpawnedSubject;
            KiDespawnSubject = kiDespawnSubject;
            InputEventSubject = inputSubject;
        }

        public int Number { get; }
        public ITenseSubject<SushiEventName> EventSubject { get; }
        public ITenseSubject<KiSpawnedParam> KiSpawnedSubject { get; }
        public ITenseSubject<string> KiDespawnSubject { get; }
        public ITenseSubject<InputEventName> InputEventSubject { get; }
    }
}