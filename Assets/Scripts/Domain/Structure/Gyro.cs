﻿using CAFU.Core;
using ExtraUniRx;

namespace Kaisen.Domain.Structure
{
    public interface IGyroStructure : IStructure
    {
        ITenseSubject<float> GyroAttitudeSubject { get; }
    }

    public struct GyroStructure : IGyroStructure
    {
        public GyroStructure(ITenseSubject<float> gyroAttitudeSubject)
        {
            GyroAttitudeSubject = gyroAttitudeSubject;
        }

        public ITenseSubject<float> GyroAttitudeSubject { get; }
    }
}