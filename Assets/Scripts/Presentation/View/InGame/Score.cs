﻿using Kaisen.Presentation.Presenter;
using UnityEngine;
using UnityEngine.UI;

namespace Kaisen.Presentation.View.Game
{
    public class Score : MonoBehaviour,
        IScoreRenderer
    {
        private Text label;
        private Text Label => label ? label : label = GetComponent<Text>();

        public void Render(int score)
        {
            Label.text = score.ToString();
        }
    }
}