﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Kaisen.Presentation.View.Game
{
    public class DebugAcceleration : UIBehaviour
    {
        [SerializeField] private Text _text;

        protected override void Start()
        {
            base.Start();

            this.UpdateAsObservable()
                .Subscribe(_ =>
                {
                    var a = Input.gyro.userAcceleration;
                    _text.text = $"x: {a.x}, y: {a.y}, z: {a.z}";
                });
        }
    }
}