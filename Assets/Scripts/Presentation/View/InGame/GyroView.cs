﻿using ExtraUniRx;
using Kaisen.Domain.Structure;
using Kaisen.Presentation.Presenter;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Kaisen.Presentation.View.Game
{
    public class GyroView : MonoBehaviour, IGyroView
    {
        [SerializeField] private Slider _slider;

        public void Initialize(IGyroStructure gyroStructure)
        {
#if UNITY_ANDROID
            Input.gyro.enabled = true;
#endif
#if UNITY_IOS
            Input.gyro.enabled = true;
#endif

            this.UpdateAsObservable()
                .Subscribe(_ =>
                {
                    // Update gyro by "roll" gesture.
                    gyroStructure.GyroAttitudeSubject.Do(Input.gyro.attitude.y);
                });
        }

        public void Render(float gyroAttitude)
        {
            _slider.value = gyroAttitude / 0.4f; // TODO: GyroEntityのInitial/Thresholdを見て基準値と判定する値を変更する
        }
    }
}