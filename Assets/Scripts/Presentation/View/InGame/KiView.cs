﻿using ExtraUniRx;
using Kaisen.Application;
using Kaisen.Presentation.Presenter;
using UniRx;
using UnityEngine;
using Zenject;

namespace Kaisen.Presentation.View.Game
{
    public class KiView : MonoBehaviour, IKiView
    {
        [SerializeField] private Animator _animator;
        private ITenseSubject<string> _kiDespawnSubject;
        private string _id = "";
        private const float Duration = 0.5f;

        [Inject]
        private void Initialize(string id, Vector2 position, Transform parent, ITenseSubject<string> kiDespawnSubject)
        {
            _kiDespawnSubject = kiDespawnSubject;
            _id = id;

            transform.SetParent(parent);
            transform.position = position;

            _kiDespawnSubject
                .WhenDo()
                .Where(despawnId => despawnId == id)
                .Subscribe(_ =>
                {
                    _animator.SetTrigger(Constant.TriggerName.KiDespawn);
                    ObservableTween.Tween(
                            () => GetComponent<Transform>().localPosition,
                            () => Vector3.zero,
                            Duration,
                            ObservableTween.EaseType.OutSinusoidal
                        )
                        .Subscribe(pos => GetComponent<Transform>().localPosition = pos)
                        .AddTo(this);
                })
                .AddTo(this);
            _kiDespawnSubject
                .WhenDid()
                .Where(despawnId => despawnId == id) // TODO: 同じ指で時間差で押した時に過去のものと一緒に削除されてしまう
                .Subscribe(_ => Destroy(gameObject))
                .AddTo(this);
        }

        // AnimationEventとして
        public void DispatchEventDid()
        {
            if (_id == "")
            {
                Debug.LogWarning("id does not exist!!!");
            }

            _kiDespawnSubject.Did(_id);
        }
    }
}