﻿using Kaisen.Presentation.Presenter;
using UnityEngine;
using UnityEngine.UI;

namespace Kaisen.Presentation.View.Game
{
    public class Timer : MonoBehaviour,
        ITimerRenderer
    {
        private Text label;
        private Text Label => label ? label : label = GetComponent<Text>();

        public void Render(string time)
        {
            Label.text = time;
        }
    }
}