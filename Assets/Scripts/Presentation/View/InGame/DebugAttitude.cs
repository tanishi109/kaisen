﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Kaisen.Presentation.View.Game
{
    public class DebugAttitude : UIBehaviour
    {
        [SerializeField] private Text _text;

        protected override void Start()
        {
            base.Start();

            this.UpdateAsObservable()
                .Subscribe(_ =>
                {
                    var a = Input.gyro.attitude;
                    _text.text = $"x: {a.x}, y: {a.y}, z: {a.z}, w: {a.w}";
                });
        }
    }
}