﻿using Kaisen.Presentation.Presenter;
using UnityEngine;
using UnityEngine.UI;

namespace Kaisen.Presentation.View.InGame
{
    [RequireComponent(typeof(Button))]
    public class ButtonBack : MonoBehaviour, IButtonTrigger
    {
    }
}