﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtraUniRx;
using Kaisen.Domain.Structure;
using Kaisen.Application;
using Kaisen.Application.Struct;
using Kaisen.Application.Enumerate;
using Kaisen.Presentation.Presenter;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Kaisen.Presentation.View.Game
{
    public class SushiView : UIBehaviour, ISushiView
    {
        [SerializeField] private List<Sprite> _numberSprites;
        [SerializeField] private List<SpriteRenderer> _digitSprites;
        [SerializeField] private Animator _animator;
        private ITenseSubject<SushiEventName> _sushiEventSubject;
        private ITenseSubject<KiSpawnedParam> _kiSpawnedSubject;
        private ITenseSubject<string> _kiDespawnSubject;

        [Inject]
        private void Initialize(ISushi sushi)
        {
            _sushiEventSubject = sushi.EventSubject;
            _kiSpawnedSubject = sushi.KiSpawnedSubject;
            _kiDespawnSubject = sushi.KiDespawnSubject;

            _sushiEventSubject
                .WhenDo()
                .Where(e => e == SushiEventName.Cooked)
                .Subscribe(_ => _animator.SetTrigger(Constant.TriggerName.Cooked));
            _sushiEventSubject
                .WhenDid()
                .Where(e => e == SushiEventName.Cooked)
                .Subscribe(_ => Destroy(gameObject));

            _sushiEventSubject
                .WhenDo()
                .Where(e => e == SushiEventName.OverCooked)
                .Subscribe(_ => _animator.SetTrigger(Constant.TriggerName.OverCooked));
            _sushiEventSubject
                .WhenDid()
                .Where(e => e == SushiEventName.OverCooked)
                .Subscribe(_ => Destroy(gameObject));

            _sushiEventSubject
                .WhenDo()
                .Where(e => e == SushiEventName.UnderCooked)
                .Subscribe(_ => _animator.SetTrigger(Constant.TriggerName.UnderCooked));
            _sushiEventSubject
                .WhenDid()
                .Where(e => e == SushiEventName.UnderCooked)
                .Subscribe(_ => Destroy(gameObject));

            this.OnPointerDownAsObservable()
                .Subscribe(pointerEventData =>
                {
                    _sushiEventSubject.Do(SushiEventName.Increment);
                    DispatchKiSpawnedSubjectDo(pointerEventData.pointerId.ToString(),
                        pointerEventData.pointerCurrentRaycast.worldPosition);
                });
            this.OnPointerUpAsObservable().Subscribe(pointerEventData =>
            {
                _sushiEventSubject.Do(SushiEventName.SubmitIncrement);
                _kiDespawnSubject.Do(pointerEventData.pointerId.ToString());
            });

            var alphabets = Enumerable.Range('a', 'z' - 'a' + 1)
                .Select(i => ((char) i).ToString())
                .ToList();

            this.UpdateAsObservable()
                .Where(_ => Input.anyKeyDown)
                .Subscribe(_ =>
                {
                    alphabets.Where(Input.GetKeyDown)
                        .ToList()
                        .ForEach(a =>
                        {
                            _sushiEventSubject.Do(SushiEventName.Increment);
                            DispatchKiSpawnedSubjectDo(a, Vector2.negativeInfinity);
                        });
                });

            this.UpdateAsObservable()
                .Where(_ =>
                {
                    // TODO: タップした指を残してDishUpした場合、次のスシに対してSubmitが有効になっているの直したい
                    return alphabets.Where(Input.GetKeyUp)
                               .ToList()
                               .Count > 0;
                })
                .Subscribe(_ =>
                {
                    alphabets.Where(Input.GetKeyUp)
                        .ToList()
                        .ForEach(a =>
                        {
                            sushi.EventSubject.Do(SushiEventName.SubmitIncrement);
                            _kiDespawnSubject.Do(a);
                        });
                });

            sushi.InputEventSubject
                .WhenDo()
                .Where(e => e == InputEventName.Submit)
                .Subscribe(_ => { sushi.EventSubject.Do(SushiEventName.DishUp); })
                .AddTo(this);

            // TODO: これはSushiViewにあるべきなのか？タップエリアだけのViewがやったほうがいいかも
            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyUp(KeyCode.Space))
                .Subscribe(_ => { sushi.InputEventSubject.Do(InputEventName.Submit); });
        }

        public void Render(int number)
        {
            var padding = new string('0', _digitSprites.Count);
            var spriteIndices = number.ToString(padding).Select(numberChar =>
            {
                var numberStr = numberChar.ToString();
                return int.Parse(numberStr);
            }).Reverse().ToList();

            var i = 0;
            _digitSprites.ForEach(d =>
            {
                var index = spriteIndices[i];
                var sprite = _numberSprites[index];
                d.sprite = sprite;
                var isActive = i == 0 || number >= Math.Pow(10, i);
                d.gameObject.SetActive(isActive);
                i++;
            });
        }

        // AnimationEventとして呼ぶ
        public void DispatchEventDid(SushiEventName eventName)
        {
            _sushiEventSubject.Did(eventName);
        }

        private void DispatchKiSpawnedSubjectDo(string id, Vector2 position)
        {
            var param = new KiSpawnedParam(id, position, transform, _kiDespawnSubject);
            _kiSpawnedSubject.Do(param);
        }
    }
}