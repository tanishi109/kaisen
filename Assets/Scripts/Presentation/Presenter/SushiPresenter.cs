﻿using Kaisen.Domain.Structure;
using Kaisen.Domain.UseCase;
using Zenject;

namespace Kaisen.Presentation.Presenter
{
    public class SushiPresenter : ISushiPresenter
    {
        [Inject] private IFactory<ISushi, ISushiView> SushiViewFactory { get; set; }
        private ISushiView SushiView;

        public void Instantiate(ISushi sushi)
        {
            SushiView = SushiViewFactory.Create(sushi);
        }

        public void RenderNumber(int number)
        {
            SushiView.Render(number);
        }
    }
}