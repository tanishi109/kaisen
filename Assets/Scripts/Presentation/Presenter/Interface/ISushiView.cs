﻿using CAFU.Core;

namespace Kaisen.Presentation.Presenter
{
    public interface ISushiView : IView
    {
        void Render(int number);
    }
}