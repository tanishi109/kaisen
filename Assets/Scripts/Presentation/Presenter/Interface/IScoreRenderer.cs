﻿using CAFU.Core;

namespace Kaisen.Presentation.Presenter
{
    public interface IScoreRenderer : IView
    {
        void Render(int score);
    }
}