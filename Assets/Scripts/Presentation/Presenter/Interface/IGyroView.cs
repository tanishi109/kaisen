﻿using CAFU.Core;
using Kaisen.Domain.Structure;

namespace Kaisen.Presentation.Presenter
{
    public interface IGyroView : IView
    {
        void Initialize(IGyroStructure gyroStructure);
        void Render(float gyroAttitude);
    }
}