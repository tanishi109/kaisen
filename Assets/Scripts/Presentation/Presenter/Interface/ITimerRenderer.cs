﻿using CAFU.Core;

namespace Kaisen.Presentation.Presenter
{
    public interface ITimerRenderer : IView
    {
        void Render(string time);
    }
}