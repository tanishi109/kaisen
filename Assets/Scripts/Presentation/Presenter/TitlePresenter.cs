﻿using System;
using Kaisen.Application;
using Kaisen.Domain.UseCase;
using UniRx;
using Zenject;

namespace Kaisen.Presentation.Presenter
{
    public class TitlePresenter : ITitleNavigator
    {
        [Inject(Id = Constant.InjectId.ButtonStart)]
        private IButtonTrigger TriggerStart { get; set; }

        [Inject(Id = Constant.InjectId.ButtonStartMulti)]
        private IButtonTrigger TriggerStartMulti { get; set; }

        public IObservable<Unit> OnNavigateToInGameAsObservable()
        {
            return TriggerStart.OnTriggerAsObservable();
        }

        public IObservable<Unit> OnNavigateToPrepareMultiAsObservable()
        {
            return TriggerStartMulti.OnTriggerAsObservable();
        }
    }
}