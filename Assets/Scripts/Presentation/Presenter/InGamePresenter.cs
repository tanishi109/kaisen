﻿using System;
using Kaisen.Application;
using Kaisen.Domain.UseCase;
using UniRx;
using Zenject;

namespace Kaisen.Presentation.Presenter
{
    public class InGamePresenter :
        IGameScoreRenderable,
        ITimerRenderable,
        IInGameNavigator
    {
        [Inject] private IScoreRenderer ScoreRenderer { get; set; }
        [Inject] private ITimerRenderer TimerRenderer { get; set; }

        [Inject(Id = Constant.InjectId.ButtonBack)]
        private IButtonTrigger TriggerBack { get; set; }

        public void RenderScore(int score)
        {
            ScoreRenderer.Render(score);
        }

        public void RenderTimer(float time)
        {
            TimerRenderer.Render(time.ToString());
        }

        public IObservable<Unit> OnNavigateToTitleAsObservable()
        {
            return TriggerBack.OnTriggerAsObservable();
        }
    }
}