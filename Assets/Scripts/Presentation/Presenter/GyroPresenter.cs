﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtraUniRx;
using Kaisen.Application.Struct;
using Kaisen.Domain.Structure;
using Kaisen.Domain.UseCase;
using UnityEngine;
using Zenject;

namespace Kaisen.Presentation.Presenter
{
    public class GyroPresenter : IGyroPresenter
    {
        [Inject] private IGyroView GyroView { get; set; }

        public void Instantiate(IGyroStructure gyroStructure)
        {
            GyroView.Initialize(gyroStructure);
        }

        public void Render(float gyroAttitude)
        {
            GyroView.Render(gyroAttitude);
        }
    }
}