﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtraUniRx;
using Kaisen.Application.Struct;
using Kaisen.Domain.UseCase;
using UnityEngine;
using Zenject;

namespace Kaisen.Presentation.Presenter
{
    public class KiPresenter : IKiRenderable
    {
        [Inject]
        private IFactory<string, Vector2, Transform, ITenseSubject<string>, IKiView> KiViewFactory { get; set; }

        private static readonly List<List<string>> Qwerty = new List<List<string>>
        {
            new List<string> {"q", "w", "e", "r", "t", "y", "u", "i", "o", "p"},
            new List<string> {"a", "s", "d", "f", "g", "h", "j", "k", "l"},
            new List<string> {"z", "x", "c", "v", "b", "n", "m"}
        };

        // a行はq行よりキー0.25個分くらい左側にマージンがある, z行はq行よりキー0.75個分くらい左側にマージンがある
        private static readonly List<float> LeftMargin = new List<float> {0.0f, 0.25f, 0.75f};
        private static readonly float TopMarginRate = 0.2f;

        public void RenderKi(KiSpawnedParam param)
        {
            if (float.IsInfinity(param.Position.x) || float.IsInfinity(param.Position.y))
            {
                // TODO: IDがa~zじゃないとき
                param.Position = QwertyToVector2(param.Id);
            }

            KiViewFactory.Create(param.Id, param.Position, param.Parent, param.KiDespawnSubject);
            // TODO: KiSpawned を InGameEventにしてみる？
        }

        public Vector2 QwertyToVector2(string str)
        {
            // TODO: このカメラの取り方やめる
            var camera = Camera.main;
            if (camera == default(Camera))
            {
                Debug.LogWarning("Main camera not found.");
                return Vector2.zero;
            }

            var leftTop = camera.ScreenToWorldPoint(new Vector3(0, camera.pixelHeight, camera.nearClipPlane));
            var worldWidth = (Math.Abs(leftTop.x) * 2);
            var keySize = worldWidth / Qwerty[0].Count;
            var row = Qwerty.FindIndex(list => list.Contains(str));

            if (Qwerty.ElementAtOrDefault(row) == default(List<string>))
            {
                Debug.LogWarning("row not found. id should be a ~ z.");
                return Vector2.zero;
            }

            var column = Qwerty[row].FindIndex(keyTopString => keyTopString.Equals(str));
            var worldHeight = (Math.Abs(leftTop.y) * 2);

            return new Vector2(leftTop.x + column * keySize + LeftMargin[row] * keySize,
                leftTop.y - row * keySize - TopMarginRate * worldHeight);
        }
    }
}